<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['as' => 'api.'], function(){
    Route::apiResource('company', 'CompanyController');
    Route::apiResource('users', 'UserController');
});

Route::get('company/restore/{id}', 'CompanyController@restore');
Route::get('users/restore/{id}', 'UserController@restore');

Route::get('company/work/{id}', 'CompanyController@work')->name('company.work');
Route::get('user/work/{id}', 'UserController@work')->name('user.work');

Route::post('company/work_update/{id}', 'CompanyController@work_update');
Route::post('user/work_update/{id}', 'UserController@work_update');
