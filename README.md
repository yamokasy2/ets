# ETS International

Что требуется сделать:
1. База данных:

	Создать таблицу Компания
	Создать таблице Пользователь
	Создать формулизацию данных таблиц. Так как может быть множество пользователей в одной компании. И множество компаний может быть у одного пользователя
	
	У таблицы "Компания" существуют такие атрибуты как: CompanyID, CompanyName
	
	У таблицы "Пользователь" существуют такие атрибуты как UserID, UserName
	
2. Релизовать API, которое будет выполнять действия CRUD. У обоих моделей.



# ИНСТРУКЦИЯ

Настройка проекта
Composer:

	Composer isntall

ENV(DB)

	DB_CONNECTION=pgsql
	DB_HOST=localhost
	DB_PORT=5432
	DB_DATABASE=ets
	DB_USERNAME=postgres
	DB_PASSWORD=secret

Console Command

		php artisan migration
		php aritsan migrate:fresh --seed 	

CRUD
 
	Пользователя(users)
		1. api/users(GET) - Список пользователей
		2. api/users/{ID}(GET) - Список пользователя
		3. api/user{POST} - Добавление пользователя(Параметр UserName, для form-data)
		4  api/user/{ID}(PUT) - Обновление пользователя (параметр UserName, для x-www-form-urlencoded)
		5. api/user/{ID}(DELETE) - Мягкое удаление пользователя.
		6. api/user/restore/{ID}(GET) - Восстановление пользователя.
 	Фирмы(companies)
		1. api/company(GET) - Список фирм
		2. api/company/{ID}(GET) - Список фирмы
		3. api/company{POST} - Добавление фирмы(Параметр CompanyName, для form-data)
		4  api/company/{ID}(PUT) - Обновление фирмы (параметр CompanyName, для x-www-form-urlencoded)
		5. api/company/{ID}(DELETE) - Мягкое удаление фирмы.
		6. api/company/restore/{ID}(GET) - Восстановление фирмы.
 	Работы
		1. api/user/work/{ID}(GET) - Список фирм по номеру пользователя
		2. api/company/work/{ID}(GET) - Список пользователей по номеру фирмы
		3. api/company/work_update/{ID}(POST) - Создать связь мужду фирмой и пользователями( UserID = (array) )
		4. api/users/work_update/{ID}(POST) - Создать связь мужду пользователем и фирмами( CompanyID = (array) )
		

--

Ход работы:
 
 Реализованы модели: Company, User, Work;
 
 Реализованы контроллеры: CompanyController, UserController; 
 
 ( Можно сделать одним контроллером( так как идентичны методы и алгоритмы). Также есть идеи по поводу реализации репозитории для ДДД)
 
 Реализованы Request: UserRequest, CompanyRequest;
 
 Реализация API( resource: company, user. EndPoints: users/work, users/work_update, company/work, company/work_update);
 	
--	
 

Version Laravel: 5.7

Version PostrgeSQL 10.5

UML схема: http://prntscr.com/oj3bc0

-- 

Спасибо за уделенное внимание!
