<?php

namespace App\Http\Requests;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|null
     */
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return ['CompanyName' => 'required|string'];
            case 'PUT':
                return [
                        'CompanyID' => 'unique:companies,CompanyID',
                        'CompanyName' => 'required|string',
                    ];
            case 'DELETE':
                return [
                    'CompanyID' => 'required|integer|exists:companies,CompanyID'
                ];
            default:
                return null;
        }

    }

    public function messages()
    {
        return [
            'CompanyName.required' => 'Название пользователя не должно быть пустым',
            'CompanyID.exists'  => 'Таких данных нет',
            'CompanyID.integer'  => 'Должен быть номер(int)',
            'CompanyID.unique'  => 'Такой номер уже существует!',
            'CompanyName.unique'  => 'Такая компания уже есть!',
        ];
    }

}
