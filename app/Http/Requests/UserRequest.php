<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $def_rule = ['UserName' => 'required|string'];
        switch ($this->getMethod())
        {
            case 'POST':
                return $def_rule;
            case 'PUT':
                return [
                    'UserID' => 'unique:users,UserID',
                    //'UserName' => 'required|string|uniquer:users,UserName'
                ];
            case 'DELETE':
                return [
                    'UserID' => 'required|integer|exists:users,UserID'
                ];
            default:
                return null;
        }

    }


    public function messages()
    {
        return [
            'UserName.required' => 'Название пользователя не должно быть пустым',
            'UserID.exists'  => 'Таких данных нет',
            'UserID.integer'  => 'Должен быть номер(int)',
            'UserID.unique'  => 'Такой номер уже существует!',
            'UserName.unique'  => 'Такая компания уже есть!',
        ];
    }
}
