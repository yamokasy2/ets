<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use App\Models\User;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Models\Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Company::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $companyRequest)
    {
        $company = new Company;
        $company->fill($companyRequest->validated());
        $company->save();

        return response()->json($company, 201);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company $company
     *
     * @return \App\Models\Company
     */
    public function show(Company $company)
    {
        return $company;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $companyRequest, Company $company)
    {
        $company->fill($companyRequest->only(['CompanyName']));
        $company->save();

        return response()->json($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        if ($company->delete()) {
            return response(null, 204);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $status = Company::withTrashed()->find($id)->restore();

        return response()->json(['restore' => $status?true:false], 200);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function work($id)
    {
        $company = Company::FindOrFail($id);

        return $company->work()->get()->map(function ($item) {
            return $item->users;# Выводим юзеров по текущей фирме
        });
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function work_update($id, Request $request)
    {
        $users = $request->UserID; # Список пользователей

        # Очищаем старые связи
        DB::table('work')->where('CompanyID', '=', $id)->delete();

        # Работа с массивом пользователей(идов)
        $items = collect($users)
            ->reject(function($user_id){
                return !User::find($user_id); # Убираем те, которых не существует в таблице Юзеров
            })
            ->map(function ($user_id) use ($id) {
            $work = Work::firstOrNew([
                'UserID' => $user_id,
                'CompanyID' => $id,
            ]);
            $work->save();# Сохраняем

            return $work;
        });

        return [
            'status' => true,
            'msg'    => 'Created relationships company-user. By company',
            'items'  => $items->toArray(),
        ];
    }
}
