<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Company;
use App\Models\User;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Models\User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::all() ?: ['error' => ['code' => 1, 'msg' => 'Пусто']];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $userRequest)
    {
        $user = new User;
        $user->fill($userRequest->validated());
        $user->save();

        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     *
     * @return \App\Models\User
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $userRequest, User $user)
    {
        $user->fill($userRequest->only(['UserName']));
        $user->save();

        return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        return response()->json(['destroy' => ($user->delete()) ? true : false],
            204)->getContent();
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $status = User::withTrashed()->find($id)->restore();

        return response()->json(['restore' => $status?true:false], 200);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function work($id)
    {
        $user = User::FindOrFail($id);

        return $user->work()->get()->map(function ($item) {
            return $item->companies;
        });
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function work_update($id, Request $request)
    {
        $companies = $request->CompanyID; # Список Фирм(ид)
        DB::table('work')->where('UserID', '=', $id)->delete(); # Очищаем старые
        $items = collect($companies)->reject(function ($company_id) {
                return ! Company::find($company_id);# УБираем не существующих
            })->map(function ($company_id) use ($id) {
                $work = Work::firstOrNew([
                    'UserID'    => $id,
                    'CompanyID' => $company_id,
                ]);
                $work->save();# Заполняем

                return $work;
            });

        return [
            'status' => $companies ? true : false,
            'msg'    => 'Created relationships company-user. By user',
            'items'  => $items->toArray(),
        ];
    }
}
