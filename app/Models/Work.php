<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{

    protected $table = 'work'; # Таблица
    protected $fillable = ['CompanyID', 'UserID'];  # Рабочие
    protected $primaryKey = ['CompanyID', 'UserID']; # Для many-to-many Двя составных ключа они же и первичные
    public $incrementing = false;  # Убираем аутоинкримент

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Models\User', 'UserID', 'UserID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companies()
    {
        return $this->belongsTo('App\Models\Company', 'CompanyID', 'CompanyID');
    }
}
