<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $table = 'users'; # Таблица
    protected  $primaryKey = 'UserID'; # Первичный ключ (он не id)
    protected $fillable = ['UserName']; # Заполняемые
    public $timestamps = false; # Убираем таймспавн

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function work()
    {
        return $this->hasMany('App\Models\Work', 'UserID', 'UserID');
    }
}
