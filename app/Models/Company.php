<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = 'companies'; # Таблица
    protected  $primaryKey = 'CompanyID'; # PK
    protected $fillable = ['CompanyName']; # Заполняемые
    public $timestamps = false; # УБираем таймспан

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function work()
    {
        return $this->hasMany('App\Models\Work', 'CompanyID', 'CompanyID');
    }
}
