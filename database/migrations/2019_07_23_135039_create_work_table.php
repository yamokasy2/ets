<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work', function (Blueprint $table) {

            $table->integer('UserID')->unsigned();
            $table->integer('CompanyID')->unsigned();

            $table->foreign('UserID')
                ->references('UserID')->on('users')
                ->onDelete('cascade');

            $table->foreign('CompanyID')
                ->references('CompanyID')->on('companies')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work');
    }
}
