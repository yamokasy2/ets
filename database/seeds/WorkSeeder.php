<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         * Это просто рандомное время.
         * date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
         *
         * */

        DB::table('work')->insert(array(
            [
                'UserID' => 1,
                'CompanyID' => 2,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 1,
                'CompanyID' => 3,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 2,
                'CompanyID' => 3,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 2,
                'CompanyID' => 2,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 3,
                'CompanyID' => 1,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 4,
                'CompanyID' => 3,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 6,
                'CompanyID' => 3,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
            [
                'UserID' => 5,
                'CompanyID' => 3,
                'created_at' => date('Y-'.rand(1,12).'-'.rand(1,20).' '.rand(1,23).':'.rand(1,59).':'.rand(1,59))
            ],
        ));
    }
}
