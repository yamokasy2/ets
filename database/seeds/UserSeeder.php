<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert(array(
            [ 'UserName' => 'Иван'],
            [ 'UserName' => 'Пётр'],
            [ 'UserName' => 'Александр'],
            [ 'UserName' => 'Глеб'],
            [ 'UserName' => 'Сергей'],
            [ 'UserName' => 'Владислав'],
        ));
    }
}
