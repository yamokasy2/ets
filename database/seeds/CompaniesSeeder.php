<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Просто демо записи. Чтобы видно было, что знаем сиды. и миграции.
        DB::table('companies')->insert(array(
            [
                'CompanyID' => 1,
                'CompanyName' => 'Apple'
            ],
            [
                'CompanyID' => 2,
                'CompanyName' => 'Google'
            ],
            [
                'CompanyID' => 3,
                'CompanyName' => 'ETS'
            ],
        ));
    }
}
